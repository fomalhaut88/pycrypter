import sys
from Tkinter import Tk
from models import Console, Gui


if __name__ == "__main__":
	if len(sys.argv) == 1:
		root = Tk()
		gui = Gui(root)
		root.mainloop()

	else:
		console = Console()
		console.execute(sys.argv)