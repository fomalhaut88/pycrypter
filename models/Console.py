from models import PyCrypter



class Console(object):
	def _parse_argv(self, argv):
		args = {}

		if len(argv) > 1:
			args['action'] = argv[1]

			for i in xrange((len(argv) - 2) / 2):
				args[argv[2 + 2 * i]] = argv[3 + 2 * i]

		return args


	def help():
		print "Examples:"
		print "python pycrypter.py new"
		print "python pycrypter.py encrypt -m \"your message\" -k IJRpjqQWo/yn+yLcDBN525d/fdENo2"
		print "python pycrypter.py decrypt -m Fhe94jy0ZnN+31mh3pd3h -k Ft4K7rx1U7w912jAW2EMPCiHOKDxIjmgY7WpE2qlBhdIrdya"


	def execute(self, argv):
		args = self._parse_argv(argv)
		pycrypter = PyCrypter()

		if len(args) > 0:
			action = args['action']

			if action == 'new':
				keys = pycrypter.new()
				print "PUBLIC: %s" % keys['public']
				print
				print "PRIVATE: %s" % keys['private']
				print

			elif action == 'encrypt':
				msg = pycrypter.encrypt(args['-k'], args['-m'])
				print msg

			elif action == 'decrypt':
				msg = pycrypter.decrypt(args['-k'], args['-m'])
				print msg

			else:
				self.help()

		else:
			self.help()