import re
from base64 import b64encode, b64decode
from Crypto.PublicKey import RSA
from Crypto import Random



class PyCrypter(object):
	SIZE = 1024


	def new(self):
		random_generator = Random.new().read
		rsa = RSA.generate(self.SIZE, random_generator)
		public_key = rsa.publickey()

		private_str = re.match(r'^-----BEGIN RSA PRIVATE KEY-----\n(.*?)\n-----END RSA PRIVATE KEY-----$', rsa.exportKey(), flags=re.DOTALL).group(1).replace('\n', '-')
		public_str = re.match(r'^-----BEGIN PUBLIC KEY-----\n(.*?)\n-----END PUBLIC KEY-----$', public_key.exportKey(), flags=re.DOTALL).group(1).replace('\n', '-')
		
		return {
			'public': public_str,
			'private': private_str
		}


	def encrypt(self, key, message):
		public_str = "-----BEGIN PUBLIC KEY-----\n%s\n-----END PUBLIC KEY-----" % key.replace('-', '\n')
		rsa = RSA.importKey(public_str).publickey()

		encrypted = ""
		message_str = message
		while message_str:
			if len(message_str) > 127:
				part = message_str[:127]
				message_str = message_str[127:]
			else:
				part = message_str
				message_str = ""

			encrypted += b64encode(rsa.encrypt(part, 32)[0])

		return encrypted


	def decrypt(self, key, message):
		private_str = "-----BEGIN RSA PRIVATE KEY-----\n%s\n-----END RSA PRIVATE KEY-----" % key.replace('-', '\n')
		rsa = RSA.importKey(private_str)

		decrypted = ""
		message_str = message
		while message_str:
			if len(message_str) > 172:
				part = message_str[:172]
				message_str = message_str[172:]
			else:
				part = message_str
				message_str = ""

			decrypted += rsa.decrypt(b64decode(part))
			
		return decrypted