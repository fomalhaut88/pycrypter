from Tkinter import BOTH, Text, W, N, E, S, END
from ttk import Frame, Style, Label, Entry, Button

from models import PyCrypter



class Gui(Frame):
	def __init__(self, parent):
		Frame.__init__(self, parent)
		self.parent = parent
		self.pycrypter = PyCrypter()

		self.__load_parent()
		self.__load_elements()


	def __load_parent(self):
		self.parent.geometry("634x360+100+100")
		self.parent.title("PyCrypter")
		self.style = Style()
		self.style.theme_use('default')
		self.pack(fill=BOTH, expand=1)


	def __load_elements(self):
		self.columnconfigure(0, pad=3, minsize=100)
		self.columnconfigure(1, pad=3)
		self.columnconfigure(2, pad=3)
		self.rowconfigure(0, pad=3)
		self.rowconfigure(1, pad=3)
		self.rowconfigure(2, pad=3)
		self.rowconfigure(3, pad=3)
		self.rowconfigure(4, pad=3)
		self.rowconfigure(5, pad=3)
		self.rowconfigure(6, pad=3)
		self.rowconfigure(7, pad=3)
		self.rowconfigure(8, pad=3)

		# first
		self.lbl_generate = Label(self, text="Generate a pair of public and private keys here.")
		self.lbl_generate.grid(row=0, column=0, columnspan=3, sticky=W+S, padx=7, pady=7)

		self.bnt_generate = Button(self, text="Generate", command=self.__bnt_generate_click)
		self.bnt_generate.grid(row=1, column=0, rowspan=2, sticky=N+S+W+E, padx=7, pady=7)

		self.entry_public = Entry(self)
		self.entry_public.grid(row=1, column=1, sticky=S+W+E, pady=4)

		self.entry_private = Entry(self)
		self.entry_private.grid(row=2, column=1, sticky=N+W+E, pady=4)

		self.bnt_to_public = Button(self, text="to public", command=self.__bnt_to_public_click)
		self.bnt_to_public.grid(row=1, column=2, sticky=S+W, padx=7, pady=2)

		self.bnt_to_private = Button(self, text="to private", command=self.__bnt_to_private_click)
		self.bnt_to_private.grid(row=2, column=2, sticky=N+W, padx=7, pady=2)

		# second
		self.lbl_public = Label(self, text="Encrypt text with public key.")
		self.lbl_public.grid(row=3, column=0, columnspan=3, sticky=W, padx=7, pady=7)

		self.area_public_from = Text(self, width=30, height=5)
		self.area_public_from.grid(row=4, column=0, rowspan=2, padx=7, pady=7)

		self.area_public_to = Text(self, width=30, height=5)
		self.area_public_to.grid(row=4, column=2, rowspan=2, padx=7, pady=7)

		self.entry_public_msg = Entry(self)
		self.entry_public_msg.insert(0, "enter a public key here")
		self.entry_public_msg.grid(row=4, column=1, sticky=S+W+E, pady=4)

		self.bnt_public_msg = Button(self, text="Encrypt", command=self.__bnt_public_msg_click)
		self.bnt_public_msg.grid(row=5, column=1, sticky=N+W+E, pady=4)

		# third
		self.lbl_private = Label(self, text="Decrypt text with private key.")
		self.lbl_private.grid(row=6, column=0, columnspan=3, sticky=W, padx=7, pady=7)

		self.area_private_from = Text(self, width=30, height=5)
		self.area_private_from.grid(row=7, column=0, rowspan=2, padx=7, pady=7)

		self.area_private_to = Text(self, width=30, height=5)
		self.area_private_to.grid(row=7, column=2, rowspan=2, padx=7, pady=7)

		self.entry_private_msg = Entry(self)
		self.entry_private_msg.insert(0, "enter a private key here")
		self.entry_private_msg.grid(row=7, column=1, sticky=S+W+E, pady=4)

		self.bnt_private_msg = Button(self, text="Decrypt", command=self.__bnt_private_msg_click)
		self.bnt_private_msg.grid(row=8, column=1, sticky=N+W+E, pady=4)


	def __bnt_generate_click(self):
		keys = self.pycrypter.new()

		self.entry_public.delete(0, END)
		self.entry_public.insert(0, keys['public'])

		self.entry_private.delete(0, END)
		self.entry_private.insert(0, keys['private'])


	def __bnt_to_public_click(self):
		key = self.entry_public.get()
		self.entry_public_msg.delete(0, END)
		self.entry_public_msg.insert(0, key)


	def __bnt_to_private_click(self):
		key = self.entry_private.get()
		self.entry_private_msg.delete(0, END)
		self.entry_private_msg.insert(0, key)


	def __bnt_public_msg_click(self):
		key = self.entry_public_msg.get()
		msg = self.area_public_from.get(1.0, END).encode('utf8')
		encrypted = self.pycrypter.encrypt(key, msg)
		self.area_public_to.delete(1.0, END)
		self.area_public_to.insert(END, encrypted)


	def __bnt_private_msg_click(self):
		key = self.entry_private_msg.get()
		msg = self.area_private_from.get(1.0, END).encode('utf8')
		decrypted = self.pycrypter.decrypt(key, msg)
		self.area_private_to.delete(1.0, END)
		self.area_private_to.insert(END, decrypted)